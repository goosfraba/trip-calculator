<?php

namespace Goosfraba\Yellows;

/**
 * Represents a single day of the trip
 */
final class TripDay
{
    private \DateTimeImmutable $starts;
    private \DateTimeImmutable $ends;

    public function __construct(
        \DateTimeInterface $starts,
        \DateTimeInterface $ends
    ) {
        if ($ends < $starts) {
            throw new \InvalidArgumentException("\"Start\" date cannot be grater than \"end\" date.");
        }

        $this->starts = \DateTimeImmutable::createFromInterface($starts);
        $this->ends = \DateTimeImmutable::createFromInterface($ends);
    }

    /**
     * Gets the start date
     */
    public function starts(): \DateTimeImmutable
    {
        return $this->starts;
    }

    /**
     * Gets the end date
     */
    public function ends(): \DateTimeImmutable
    {
        return $this->ends;
    }

    /**
     * Calculates the full hours of the day
     */
    public function hours(): int
    {
        return floor(($this->ends->getTimestamp() - $this->starts->getTimestamp()) / 3600);
    }

    /**
     * Checks if this day is a working one
     */
    public function isWorkingDay(): bool
    {
        return $this->starts->format('N') <= 5;
    }
}

<?php

namespace Goosfraba\Yellows;

/**
 * Represents the trip
 */
final class Trip
{
    private \DateTimeImmutable $from;
    private \DateTimeImmutable $to;

    public function __construct(
        \DateTimeInterface $from,
        \DateTimeInterface $to,
        private string $country
    ) {
        if ($to < $from) {
            throw new \InvalidArgumentException("Date \"from\" cannot be greater than a date \"to\".");
        }

        $this->from = \DateTimeImmutable::createFromInterface($from);
        $this->to = \DateTimeImmutable::createFromInterface($to);
    }

    /**
     * Gets the "from" date
     */
    public function from(): \DateTimeInterface
    {
        return $this->from;
    }

    /**
     * Gets the "to" date
     */
    public function to(): \DateTimeInterface
    {
        return $this->to;
    }

    /**
     * Gets the country
     */
    public function country(): string
    {
        return $this->country;
    }

    /**
     * Creates the list of trip days
     *
     * @return TripDay[]
     */
    public function days(): array
    {
        $days = [];

        $current = $this->from;
        do {
            $next = $current->add(new \DateInterval("P1D"))->setTime(0, 0);
            $next = $next <= $this->to ? $next : $this->to;

            $days[] = new TripDay(
                $current,
                $next
            );
            $current = $next;
        } while ($current < $this->to);

        return $days;
    }

    /**
     * Trims current trip by given number of days
     */
    public function trim(int $days): self
    {
        if ($days < 1) {
            throw new \InvalidArgumentException("You can only trim by 1 or more days.");
        }

        try {
            return new self(
                $this->from->add(new \DateInterval(sprintf("P%dD", $days)))->setTime(0, 0),
                $this->to(),
                $this->country()
            );
        } catch (\Exception $e) {
            throw new \InvalidArgumentException(
                sprintf("Could not shift the trip by %d days.", $days), 0, $e
            );
        }
    }
}

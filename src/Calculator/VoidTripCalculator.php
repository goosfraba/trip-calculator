<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;

/**
 * This calculator always returns 0 (placeholder)
 */
final class VoidTripCalculator implements TripCalculator
{
    /**
     * @inheritDoc
     */
    public function calculate(Trip $trip): float
    {
        return 0;
    }
}
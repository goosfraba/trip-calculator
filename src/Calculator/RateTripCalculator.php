<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;

final class RateTripCalculator implements TripCalculator
{
    private const HUNDRED_YEARS = 36500;
    private const COUNT_DAY_HOURS = 8;

    public function __construct(
        private float $dailyRate,
        private ?int $rateDays = null,
        private ?TripCalculator $reminderTripCalculator = null
    ) {
        $this->rateDays = $rateDays ?? self::HUNDRED_YEARS;
        $this->reminderTripCalculator = $this->reminderTripCalculator ?? new VoidTripCalculator();
    }

    public function calculate(Trip $trip): float
    {
        list($workingDays, $trimDays) = $this->extractPaidDaysAndTrimDays($trip);
        $tripValue = $workingDays * $this->dailyRate;

        if (!$trimDays) {
            return $tripValue;
        }

        return $tripValue + $this->reminderTripCalculator->calculate($trip->trim($trimDays));
    }

    /**
     * Extracts number of paid days and trim days from the trip
     */
    private function extractPaidDaysAndTrimDays(Trip $trip): array
    {
        $tripDays = $trip->days();

        $paidDays = 0;
        $trimDays = 0;
        foreach ($tripDays as $tripDay) {
            if ($tripDay->isWorkingDay() && $tripDay->hours() >= self::COUNT_DAY_HOURS) {
                $paidDays++;
            }
            $trimDays++;
            if ($paidDays == $this->rateDays) {
                break;
            }
        }

        return [$paidDays, $trimDays < count($tripDays) ? $trimDays : 0];
    }
}

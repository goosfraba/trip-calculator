<?php

namespace Goosfraba\Yellows\Calculator;

/**
 * Creates the trip calculator
 */
final class TripCalculatorFactory
{
    /**
     * Creates the trip calculator
     */
    public function create(): TripCalculator
    {
        $calculator = new ByCountryTripCalculator();

        $calculator->registerCalculator(TripCalculatorFactory::plCalculator(), "PL");
        $calculator->registerCalculator(TripCalculatorFactory::deCalculator(), "DE");
        $calculator->registerCalculator(TripCalculatorFactory::gbCalculator(), "GB");
        $calculator->registerCalculator(TripCalculatorFactory::esCalculator(), "ES");

        return $calculator;
    }

    /**
     * Creates PL trip calculator
     */
    private static function plCalculator(): TripCalculator
    {
        $baseRate = 10;
        return RateTripCalculatorBuilder::create($baseRate, 7)
            ->withRate($baseRate * 2)
            ->build();
    }

    /**
     * Creates DE trip calculator
     */
    private static function deCalculator(): TripCalculator
    {
        $baseRate = 50;
        return RateTripCalculatorBuilder::create($baseRate, 7)
            ->withRate($baseRate * 1.75)
            ->build();
    }

    /**
     * Creates GB trip calculator
     */
    private static function gbCalculator(): TripCalculator
    {
        $baseRate = 75;
        return RateTripCalculatorBuilder::create($baseRate, 7)
            ->withRate($baseRate * 4)
            ->build();
    }

    /**
     * Creates ES trip calculator
     */
    private static function esCalculator(): TripCalculator
    {
        $baseRate = 150;
        return RateTripCalculatorBuilder::create($baseRate, 3)
            ->withRate($baseRate - 50, 2)
            ->withRate($baseRate - 75)
            ->build();
    }
}

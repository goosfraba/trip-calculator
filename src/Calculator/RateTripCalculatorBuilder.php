<?php

namespace Goosfraba\Yellows\Calculator;

/**
 * This class helps build an instance of RateTripCalculatorBuilder
 */
final class RateTripCalculatorBuilder
{
    /** @var array  */
    private array $rates = [];
    private bool $isComplete = false;

    public function __construct(float $dailyRate, ?int $dailyRateDays = null)
    {
        $this->withRate($dailyRate, $dailyRateDays);
    }

    /**
     * A convenience static constructor
     */
    public static function create(float $dailyRate, ?int $dailyRateDays = null): self
    {
        return new self($dailyRate, $dailyRateDays);
    }

    /**
     * Configures the daily rate for the number of days
     */
    public function withRate(float $dailyRate, ?int $dailyRateDays = null): self
    {
        if ($this->isComplete) {
            throw new \RuntimeException("Configuration is complete. Cannot add anymore rates.");
        }

        $this->rates[] = [$dailyRate, $dailyRateDays];

        if ($dailyRateDays === null) {
            $this->isComplete = true;
        }

        return $this;
    }

    /**
     * Builds the rate calculator according to this instance configuration
     */
    public function build(): TripCalculator
    {
        if (!$this->isComplete) {
            throw new \RuntimeException("Configuration is not complete. Add rate with no days limit.");
        }

        $rates = array_reverse($this->rates);
        $calculator = null;
        foreach ($rates as $rate) {
            list($dailyRate, $dailyRateDays) = $rate;
            $calculator = new RateTripCalculator($dailyRate, $dailyRateDays, $calculator);
        }

        return $calculator;
    }
}
<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;

/**
 * Represents the trip cost calculator
 */
interface TripCalculator
{
    /**
     * Calculates the cost of the trip
     */
    public function calculate(Trip $trip): float;
}

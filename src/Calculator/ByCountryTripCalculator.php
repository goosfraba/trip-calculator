<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;

/**
 * Calculates the cost of the trip based on the country
 */
final class ByCountryTripCalculator implements TripCalculator
{
    /** @var TripCalculator[] */
    private array $calculatorsByCountry = [];

    /**
     * Registers the calculator for given country
     */
    public function registerCalculator(TripCalculator $tripCalculator, $country): void
    {
        $this->calculatorsByCountry[$country] = $tripCalculator;
    }

    /**
     * @inheritDoc
     */
    public function calculate(Trip $trip): float
    {
        return $this->selectCalculator($trip)->calculate($trip);
    }

    /**
     * Selects the calculator for given trip
     */
    private function selectCalculator(Trip $trip): TripCalculator
    {
        $country = $trip->country();
        return $this->calculatorsByCountry[$country] ?? throw new \OutOfBoundsException(sprintf("Country \"%s\" is not supported by this calculator.", $country));
    }
}
# Trip Calculator example
An example of the trip cost calculator

## Usage

```php
use Goosfraba\Yellows\Calculator\TripCalculatorFactory;
use Goosfraba\Yellows\Trip;

$calculatorFactory = new TripCalculatorFactory();
$calculator = $calculatorFactory->create();

$trip = new Trip(
    date_create_immutable("now - 5 days"),
    date_create_immutable(),
    "PL"
);

$cost = $calculator->calculate($trip);
```

## Running tests
```sh
docker-compose run php ./vendor/bin/phpunit
```

<?php

namespace Goosfraba\Yellows;

use PHPUnit\Framework\TestCase;

class TripDayTest extends TestCase
{
    /**
     * @test
     */
    public function itCannotBeConstructedWithInvalidDates()
    {
        $this->expectException(\InvalidArgumentException::class);
        new TripDay(date_create_immutable(), date_create_immutable("now - 2 days"));
    }

    /**
     * @test
     * @dataProvider tripDays
     */
    public function itCalculatesFullHours(TripDay $day, int $expectedHours)
    {
        $this->assertEquals($expectedHours, $day->hours());
    }

    public function tripDays()
    {
        return [
            '1 hour' => [
                new TripDay(
                    new \DateTimeImmutable("2022-01-23 22:32:34"),
                    new \DateTimeImmutable("2022-01-24 00:00:00")
                ),
                1
            ],
            'full day' => [
                new TripDay(
                    new \DateTimeImmutable("2022-01-23 00:00:00"),
                    new \DateTimeImmutable("2022-01-24 00:00:00")
                ),
                24
            ]
        ];
    }
}

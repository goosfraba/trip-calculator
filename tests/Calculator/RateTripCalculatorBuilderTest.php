<?php

namespace Goosfraba\Yellows\Calculator;

use http\Exception\RuntimeException;
use PHPUnit\Framework\TestCase;

class RateTripCalculatorBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function itBuildsCalculator(): void
    {
        $calculator = RateTripCalculatorBuilder::create(100)->build();
        $this->assertInstanceOf(TripCalculator::class, $calculator);
    }

    /**
     * @test
     */
    public function itPreventsCreationOfNotCompletedCalculator()
    {
        $this->expectException(\RuntimeException::class);
        RateTripCalculatorBuilder::create(100, 7)->build();
    }

    /**
     * @test
     */
    public function itPreventsModificationOfCompleteCalculator()
    {
        $this->expectException(\RuntimeException::class);
        RateTripCalculatorBuilder::create(100)
            ->withRate(200)
            ->build();
    }
}
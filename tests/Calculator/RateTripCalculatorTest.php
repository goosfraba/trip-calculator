<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class RateTripCalculatorTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @test
     */
    public function itCalculatesRatesWithReminder()
    {
        $inner = $this->prophesize(TripCalculator::class);
        $calculator = new RateTripCalculator(10, 2, $inner->reveal());

        $trip = new Trip(
            date_create_immutable("2022-11-14 08:00:00"),
            date_create_immutable("2022-11-17 16:00:00"),
            "PL"
        );

        $inner->calculate($trip->trim(2))->willReturn($reminderCost = 15.2);

        // 2 days * 10 + reminder calculator
        $this->assertEquals(20 + $reminderCost, $calculator->calculate($trip));
    }

    /**
     * @test
     */
    public function itExcludesNonWorkingDaysFromReminder()
    {
        $inner = $this->prophesize(TripCalculator::class);
        $calculator = new RateTripCalculator(10, 7, $inner->reveal());

        $trip = new Trip(
            date_create_immutable("2022-11-07 08:00:00"),
            date_create_immutable("2022-11-16 16:00:00"),
            "PL"
        );

        $inner->calculate($trip->trim(9))->willReturn($reminderCost = 15.2);
        $this->assertEquals(70 + $reminderCost, $calculator->calculate($trip));
    }

    /**
     * @test
     */
    public function itCalculatesRatesWithoutReminder()
    {
        $inner = $this->prophesize(TripCalculator::class);
        $calculator = new RateTripCalculator(10, 10, $inner->reveal());

        $trip = new Trip(
            date_create_immutable("2022-11-14 08:00:00"),
            date_create_immutable("2022-11-17 16:00:00"),
            "PL"
        );

        $inner->calculate(Argument::any())->shouldNotBeCalled();

        // 4 days * 10, no reminder
        $this->assertEquals(40, $calculator->calculate($trip));
    }
}
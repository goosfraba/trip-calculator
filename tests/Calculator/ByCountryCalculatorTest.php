<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ByCountryCalculatorTest extends TestCase
{
    use ProphecyTrait;

    /** @var TripCalculator[]|\ObjectProphecy[] */
    private array $calculators = [];

    private ByCountryTripCalculator $byCountryTripCalculator;

    protected function setUp(): void
    {
        $this->calculators["ES"] = $this->prophesize(TripCalculator::class);
        $this->calculators["GB"] = $this->prophesize(TripCalculator::class);

        $this->byCountryTripCalculator = new ByCountryTripCalculator();
        foreach ($this->calculators as $country => $calculator) {
            $this->byCountryTripCalculator->registerCalculator($calculator->reveal(), $country);
        }
    }

    /**
     * @test
     */
    public function itUsesCalculatorMatchingCountryOfTheTrip()
    {
        $trip = new Trip(date_create_immutable("now -2 days"), date_create_immutable(), "GB");

        $this->calculators["GB"]->calculate($trip)->willReturn($cost = 124.23);
        $this->calculators["ES"]->calculate($trip)->shouldNotBeCalled();

        $this->assertEquals($cost, $this->byCountryTripCalculator->calculate($trip));
    }

    /**
     * @test
     */
    public function itThrowsExceptionOnUnsupportedCountry()
    {
        $this->expectException(\OutOfBoundsException::class);
        $this->byCountryTripCalculator->calculate(new Trip(date_create_immutable("now -2 days"), date_create_immutable(), "FR"));
    }
}
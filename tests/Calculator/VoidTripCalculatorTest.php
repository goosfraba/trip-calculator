<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;
use PHPUnit\Framework\TestCase;

class VoidTripCalculatorTest extends TestCase
{
    /**
     * @test
     */
    public function itAlwaysReturnsZero()
    {
        $calculator = new VoidTripCalculator();
        $this->assertEquals(
            0,
            $calculator->calculate(
                new Trip(date_create_immutable("now -2 days"), date_create_immutable(), "PL")
            )
        );
    }
}
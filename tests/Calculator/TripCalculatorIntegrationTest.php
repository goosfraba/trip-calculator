<?php

namespace Goosfraba\Yellows\Calculator;

use Goosfraba\Yellows\Trip;
use PHPUnit\Framework\TestCase;

class TripCalculatorIntegrationTest extends TestCase
{
    private TripCalculator $tripCalculator;

    protected function setUp(): void
    {
        $calculatorFactory = new TripCalculatorFactory();
        $this->tripCalculator = $calculatorFactory->create();
    }

    /**
     * @test
     * @dataProvider trips
     */
    public function itCalculatesTrip(Trip $trip, float $expected)
    {
        $this->assertEquals($expected, $this->tripCalculator->calculate($trip));
    }

    public function trips()
    {
        return [
            "4 days, ES" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-14 09:12:22"),
                    new \DateTimeImmutable("2022-11-17 10:12:22"),
                    "ES"
                ),
                550 // 3 * 150, 1 day * 100
            ],
            "2 days, ES" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-14 09:12:22"),
                    new \DateTimeImmutable("2022-11-15 10:12:22"),
                    "ES"
                ),
                300 // 2 * 150
            ],
            "14 days, ES" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-07 09:12:22"),
                    new \DateTimeImmutable("2022-11-20 10:12:22"),
                    "ES"
                ),
                1025 // 3 days * 150, 2 days * 100, 5 days * 75 (4 weekends days)
            ],
            "7 days with weekend, PL" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-11 23:12:22"), // Friday, under 8 hours
                    new \DateTimeImmutable("2022-11-17 10:12:22"),
                    "PL"
                ),
                40 // 4 days * 10 (Friday under 8 hours, 2 weekend days)
            ],
            "12 days, PL" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-07 10:12:22"),
                    new \DateTimeImmutable("2022-11-18 10:12:22"),
                    "PL"
                ),
                130 // 7 days * 10, 3 days * 20, (10 paid days, 2 weekend days)
            ],
            "12 days, DE" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-07 10:12:22"),
                    new \DateTimeImmutable("2022-11-18 10:12:22"),
                    "DE"
                ),
                612.5 // 7 days * 50, 3 * 87.5, (10 paid days, 2 weekend days)
            ],
            "12 days, GB" => [
                new Trip(
                    new \DateTimeImmutable("2022-11-07 10:12:22"),
                    new \DateTimeImmutable("2022-11-18 10:12:22"),
                    "GB"
                ),
                1425 // 5 days * 75, 5 * 300, (10 paid days, 2 weekend days)
            ]
        ];
    }
}

<?php
namespace Goosfraba\Yellows;

use PHPUnit\Framework\TestCase;

class TripTest extends TestCase
{
    /**
     * @test
     */
    public function itCannotBeConstructedWithInvalidDates()
    {
        $this->expectException(\InvalidArgumentException::class);
        new Trip(date_create_immutable(), date_create_immutable("now - 2 days"), "PL");
    }

    /**
     * @test
     * @dataProvider trips
     */
    public function itGetsTripDays(Trip $trip, $expectedDays)
    {
        $tripDays = $trip->days();

        $this->assertCount($expectedDays, $tripDays);

        // first day assertions
        $firstDay = $tripDays[0];
        $this->assertInstanceOf(TripDay::class, $firstDay);
        $this->assertEquals($trip->from(), $firstDay->starts());

        // inner days test
        for ($i = 1; $i < count($tripDays) - 2; $i++) {
            $tripDay = $tripDays[$i];
            $this->assertInstanceOf(TripDay::class, $tripDay);

            $this->assertEquals('00:00:00', $tripDay->starts()->format('H:i:s'));
            $this->assertEquals('00:00:00', $tripDay->ends()->format('H:i:s'));
            $this->assertEquals(24, $tripDay->hours());
        }

        // last day assertions
        $lastDay = $tripDays[count($tripDays) - 1];
        $this->assertInstanceOf(TripDay::class, $lastDay);
        $this->assertEquals($trip->to(), $lastDay->ends());
    }

    public function trips(): array
    {
        return [
            "4 days" => [
                new Trip(
                    date_create_immutable("2022-10-01 12:32:45"),
                    date_create_immutable("2022-10-04 10:21:07"),
            "PL"
                ),
                4
            ],
            "1 day" => [
                new Trip(
                    date_create_immutable("2022-10-01 12:32:45"),
                    date_create_immutable("2022-10-01 15:21:07"),
                    "PL"
                ),
                1
            ]
        ];
    }

    /**
     * @test
     */
    public function itTrimsTripByGivenNumberOfDays()
    {
        $trip = new Trip(
            date_create_immutable("2022-10-01 12:32:45"),
            date_create_immutable("2022-10-04 10:21:07"),
            "PL"
        );

        $this->assertEquals(
            new Trip(
                date_create_immutable("2022-10-03 00:00:00"),
                date_create_immutable("2022-10-04 10:21:07"),
                "PL"
            ),
            $trip->trim(2)
        );
    }

    /**
     * @test
     * @dataProvider invalidTrimDays
     */
    public function itCannotTrimByZeroOrLessDays(int $trimDays)
    {
        $trip = new Trip(
            date_create_immutable("2022-10-01 12:32:45"),
            date_create_immutable("2022-10-04 10:21:07"),
            "PL"
        );
        $this->expectException(\InvalidArgumentException::class);

        $trip->trim($trimDays);
    }

    public function invalidTrimDays(): array
    {
        return [
            [0],
            [-10]
        ];
    }

    /**
     * @test
     * @dataProvider invalidTrimDays
     */
    public function itCannotTrimByMoreDaysThanTripHas()
    {
        $trip = new Trip(
            date_create_immutable("2022-10-01 12:32:45"),
            date_create_immutable("2022-10-04 10:21:07"),
            "PL"
        ); // 4 days
        $this->expectException(\InvalidArgumentException::class);

        $trip->trim(5);
    }
}